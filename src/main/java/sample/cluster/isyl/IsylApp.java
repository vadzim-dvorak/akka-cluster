package sample.cluster.isyl;

import java.io.IOException;

public class IsylApp {

    public static void main(String[] args) throws IOException {
        // starting 2 frontend nodes and 3 backend nodes
        IsylMain.main(new String[] { "13400" });
    }
}
