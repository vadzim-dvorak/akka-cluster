package sample.cluster.isyl;

import akka.actor.*;
import akka.cluster.client.ClusterClientReceptionist;
import akka.routing.FromConfig;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class IsylMain {

    public static void main(String[] args) throws IOException {
        final String port = args.length > 0 ? args[0] : "0";
        final Config config = ConfigFactory.parseString("akka.remote.netty.tcp.port=" + port).
                withFallback(ConfigFactory.load("isyl"));


        ActorSystem system = ActorSystem.create("ClusterSystem", config);
        ActorRef router =
                system.actorOf(FromConfig.getInstance().props(Props.create(IsylActor.class)),
                        "router");

        ClusterClientReceptionist.get(system).registerService(router);
    }


}
