package sample.cluster.isyl;

import akka.actor.AbstractActor;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent;
import akka.cluster.Member;
import akka.cluster.MemberStatus;
import akka.dispatch.ExecutionContexts;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.http.javadsl.Http;
import akka.http.javadsl.model.*;
import akka.http.javadsl.model.headers.RawHeader;
import akka.http.javadsl.unmarshalling.Unmarshaller;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import com.itmint.protos.CloseSessionRequestProto;
import com.itmint.protos.LoginRequestProto;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import scala.concurrent.ExecutionContextExecutor;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.util.concurrent.CompletionStage;

import static akka.pattern.PatternsCS.pipe;


public class IsylActor extends AbstractActor {
    Cluster cluster = Cluster.get(getContext().system());
    LoggingAdapter log = Logging.getLogger(getContext().system(), this);
    final Http http = Http.get(context().system());
    final ExecutionContextExecutor dispatcher = context().dispatcher();
    final Materializer materializer = ActorMaterializer.create(context());

    @Override
    public void preStart() {
        cluster.subscribe(self(), ClusterEvent.MemberUp.class);
    }

    @Override
    public void postStop() {
        cluster.unsubscribe(self());
    }

    @Override
    public AbstractActor.Receive createReceive() {
        return receiveBuilder()
                .match(LoginRequestProto.LoginRequest.class, job -> {
                    System.out.println("Actor:::::: " + getSelf() + " ----------------- " +
                                       "Cluster:::::: " + getContext().system().hashCode() + " ----------------- " +
                                       "Sender::::::: " + java.net.URLDecoder.decode(getSender().toString(), "UTF-8"));
                    System.out.println();

                    String requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://webservice.kg.com/types\"><soapenv:Header/><soapenv:Body><typ:logon><logonName>"+job.getLogonName()+"</logonName><password>"+job.getPassword()+"</password></typ:logon></soapenv:Body></soapenv:Envelope>";
                    String uri = "http://127.0.0.1:8084/axis/services/SessionServiceSOAP11port";

                    Uri u = Uri.create(uri);
                    HttpRequest complexRequest =
                            HttpRequest.POST(uri)
                                    .withEntity(HttpEntities.create(ContentTypes.TEXT_XML_UTF8, requestBody))
                                    .addHeader(RawHeader.create("SOAPAction","urn:openSession"))
                                    .withProtocol(HttpProtocols.HTTP_1_0);

                    pipe(fetch(complexRequest)
                            .thenCompose(response -> Unmarshaller.entityToString().unmarshal(response.entity(), ExecutionContexts.global(), materializer))
                            .thenApply(r->{
                                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                                DocumentBuilder builder;
                                String token = "";
                                try
                                {
                                    builder = factory.newDocumentBuilder();
                                    Document document = builder.parse(new InputSource( new StringReader( r ) ));
                                    token = document.getDocumentElement().getElementsByTagName("sessionToken").item(0).getTextContent();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                return token;
                            }), dispatcher).to(getSender());

                })
                .match(CloseSessionRequestProto.CloseSessionRequest.class, job -> {
                    String requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://webservice.kg.com/types\">\n" +
                            "   <soapenv:Header/>\n" +
                            "   <soapenv:Body>\n" +
                            "      <typ:logoff>\n" +
                            "         <sessionToken>"+job.getSessionToken()+"</sessionToken>\n" +
                            "      </typ:logoff>\n" +
                            "   </soapenv:Body>\n" +
                            "</soapenv:Envelope>";

                    String uri = "http://127.0.0.1:8084/axis/services/SessionServiceSOAP11port";
                    Uri u = Uri.create(uri);
                    HttpRequest complexRequest =
                            HttpRequest.POST(uri)
                                    .withEntity(HttpEntities.create(ContentTypes.TEXT_XML_UTF8, requestBody))
                                    .addHeader(RawHeader.create("SOAPAction","urn:closeSession"))
                                    .withProtocol(HttpProtocols.HTTP_1_0);
                    pipe(fetch(complexRequest)
                            .thenCompose(response -> Unmarshaller.entityToString().unmarshal(response.entity(), ExecutionContexts.global(), materializer))
                            .thenApply(r->{
                                return "OK";
                            }), dispatcher).to(getSender());

                })
                .match(String.class, msg -> {
                    System.out.println(msg);
                })
                .matchAny(m->{
                    System.out.println("Some message: "+m);
                })
                .build();
    }




    CompletionStage<HttpResponse> fetch(HttpRequest r) {
        return http.singleRequest(r, materializer);
    }
}